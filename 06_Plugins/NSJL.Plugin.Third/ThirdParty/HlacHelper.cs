﻿using DJ.ZEF.ThirdParty;
using NSJL.DomainModel.Background.System;
using NSJL.Framework.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSJL.Plugin.Third.ThirdParty
{
    public class HlacHelper
    {
        private static readonly object locks = new object();
        private static HlacHelper manage;
        public static HlacHelper GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new HlacHelper());
            }
        }
        private CachedBuffer cache = new CachedBuffer();
        public Action<string> CallBackStr = null;
        private SerialPortHelp sp = null;


        IniFile ini = new IniFile("Config/Config.ini");
        public CommonResult Start(string portName = "COM1", int baudRate = 9600)
        {
            //portName = ini.readKey("PowerConfig", "portName");
            //baudRate =Convert.ToInt32(ini.readKey("PowerConfig", "baudRate"));
            try
            {
                if (sp == null)
                {
                    sp = new SerialPortHelp();
                    sp.CallBackAction = DataReceived;
                    return sp.Start(portName, baudRate);
                }
                return new CommonResult() { result = false, message = "初始化失败" };
            }catch (Exception ex)
            {
                return new CommonResult() { result = false, message = "串口连接异常! "+ex.Message };
            }
        }



        private byte[] callBackResult = null;
        public void DataReceived(byte[] buffer)
        {
            callBackResult = new byte[buffer.Length];
            Array.Copy(buffer, callBackResult, buffer.Length);
        }

        //control ,0xA1（命令）,0x01（通道号：1号主板）, control（0x01 :通电，0x00:断电）
        public void OpenOrCut(byte num, byte control, Action<bool?> callback)
        {
            if (sp == null)
            {
                Start();
            }
            callBackResult = null;
            var bytes = new byte[] { 0x3B, 0xB3, 0x00, 0x08, 0xA1, num, control, 0xED };
            sp.SendByte(bytes);
            Task.Factory.StartNew(() =>
            {
                while (callBackResult == null)
                {
                    sp.SendByte(bytes);
                    Thread.Sleep(1000);
                }

                if (callBackResult != null && callBackResult.Length == 8)
                {
                    if (callBackResult[6] == 0x00)
                    {
                        callback(true);
                        callBackResult = null;
                        return;
                    }
                    else
                    {
                        callback(false);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null);
                callBackResult = null;
                return;
            });
        }



        public void SetWvalue(byte control, Action<bool?, byte[]> callback)
        {
            if (sp == null)
            {
                Start();
            }
            callBackResult = null;
            var bytes = new byte[] { 0x3B, 0xB3, 0x00, 0x08, 0xA5, 0x00, control, 0xED };
            sp.SendByte(bytes);
            Task.Factory.StartNew(() =>
            {
                while (callBackResult == null)
                {
                    sp.SendByte(bytes);
                    Thread.Sleep(100);
                }

                if (callBackResult != null && callBackResult.Length == 8)
                {
                    if (callBackResult[6] == 0x00)
                    {
                        callback(true, callBackResult);
                        callBackResult = null;
                        return;
                    }
                    else
                    {
                        callback(false, callBackResult);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null, null);
                callBackResult = null;
            });
        }


        public void SetMaxW(byte high, byte low, Action<bool?, byte[]> callback)
        {
            if (sp == null)
            {
                Start();
            }
            callBackResult = null;
            var bytes = new byte[] { 0x3B, 0xB3, 0xA6, 0x00, 0x08, high, low, 0xED };
            sp.SendByte(bytes);
            Task.Factory.StartNew(() =>
            {
                while (callBackResult == null)
                {
                    sp.SendByte(bytes);
                    Thread.Sleep(100);
                }

                if (callBackResult != null && callBackResult.Length == 8)
                {
                    if (callBackResult[6] == 0x00)
                    {
                        callback(true, callBackResult);
                        callBackResult = null;
                        return;
                    }
                    else
                    {
                        callback(false, callBackResult);
                        callBackResult = null;
                        return;
                    }
                }
                callback(null, null);
                callBackResult = null;
            });
        }


        public void Close()
        {
            callBackResult = null;
            if (sp != null)
            {
                sp.Close();
                sp = null;
            }
        }
    }
}
