﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.System
{
    public class CommonResult<T>
    {
        public bool result { get; set; }
        public string message { get; set; }
        public T data { get; set; }
    }

    public class CommonResult
    {
        public bool result { get; set; }
        public string message { get; set; }
        public string data { get; set; }
    }
}
