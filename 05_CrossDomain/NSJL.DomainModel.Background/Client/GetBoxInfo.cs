﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DomainModel.Background.Client
{
    public class GetBoxInfo
    {
        public string Id { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        //箱门大小   小   中   大
        public string BoxType { get; set; }
        //是否锁定   默认false
        public bool? IsLock { get; set; }
        //借出的用户ID  为null 空表示未借出
        public string UserId { get; set; }


        //清箱清理以下字段-------------------------
        //是否绑定物品 默认false
        //是否空箱   默认为true
        public bool? IsFree { get; set; }
        public bool? IsBind { get; set; }
        //标签号
        public string GoodsId { get; set; }
        //物资编号
        public string GoodsNum { get; set; }
        //物资分类
        public string GoodsClasss { get; set; }
        //绑定物品名称
        public string GoodsName { get; set; }
        //绑定物品时间
        public DateTime? BindTime { get; set; }
        //------------------------------------------


        //最后开门时间
        public DateTime? UpdateTime { get; set; }
        //绑定时间
        public DateTime? CreateTime { get; set; }
        //柜号名称
        public string CabName { get; set; }

        public List<string> GoodsClass { get; set; }

        public string Name { get; set; }
        public string Mobile { get; set; }
    }
    public class GetBoxWithExcel
    {
        public string CabName { get; set; }
        public int BoxNum { get; set; }
        public string GoodsName { get; set; }
        //public string GoodsId { get; set; }
        public string GoodsNum { get; set; }
        public string GoodsClasss { get; set; }
    }
    public class GetBoxWithExcelOther
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Code { get; set; }
        public string GoodsClasss { get; set; }
        public string Features { get; set; }
        public DateTime? CreateTime { get; set; }
    }
}
