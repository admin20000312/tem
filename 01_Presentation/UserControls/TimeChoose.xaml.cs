﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.UserControls
{
    /// <summary>
    /// TimeChoose.xaml 的交互逻辑
    /// </summary>
    public partial class TimeChoose : UserControl
    {
        public TimeChoose()
        {
            InitializeComponent();
            wp1.Children.Clear();
            for (int i = 0; i <= 23; i++)
            {
                TimeItem item = new TimeItem();
                item.Width = 194;
                item.Height = 48;
                item.HorizontalAlignment = HorizontalAlignment.Center;
                item.Margin = new Thickness(39,0,0,0);
                item.lab1.Content = i.ToString().PadLeft(2,'0') +":"+"00";
                item.MouseDown += Item_MouseDown;
                wp1.Children.Add(item);
            }
        }

        private void Item_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TimeItem timeItem = sender as TimeItem;
            timeItem.border2.Visibility = Visibility.Collapsed;
            timeItem.border1.BorderThickness = new Thickness (0,0,0,1);
            lab1.Content = timeItem.lab1.Content;
            g1.Tag = "1";
            Grid_MouseDown(null,null);
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var tag = g1.Tag.ToString();
            if (tag == "0")
            {
                this.Height = 341;
                g1.Tag = "1";
                g2.Visibility = Visibility.Visible;
            }
            else
            {
                this.Height = 55;
                g1.Tag = "0";
                g2.Visibility = Visibility.Collapsed;
            }
        }

        private void g2_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var childrens = wp1.Children;
            foreach (var child in childrens)
            {
                TimeItem timeItem = child as TimeItem;
                timeItem.border2.Visibility = Visibility.Visible;
                timeItem.border1.BorderThickness = new Thickness(0, 0, 0, 0);
            }
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
