﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DJ.Clients;
using DJ.Dialog;
using DJ.Pages;
using DJ.ZEF.ThirdParty;
using NSJL.Framework.Utils;

namespace DJ
{
    /// <summary>
    /// Main.xaml 的交互逻辑
    /// </summary>
    public partial class Main : Window
    {
        private static readonly object locks = new object();
        private static Main manage;
        public static Main GetInstance()
        {
            lock (locks)
            {
                return manage ?? (manage = new Main());
            }
        }
        public Main()
        {
            //WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();
            ComWindow.Start(this);
            //登录修改用户名
            Label1.Content = UserName;
            //初始化page
            Button1_Click(Button1, null);
            page5.action = (()=>
            {
                page1.Init();
                //page8.Init();
            });
        }
        public static string UserName = null;
        //返回主页
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Hide();
            //this.Close();
        }
        //退出系统
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                Environment.Exit(0);
            }
            catch (Exception exception)
            {
                TextLogUtil.Info(exception.Message);
                Environment.Exit(0);
            }
        }
        public BoxPage page1=new BoxPage();
        public ManagerPage page2 =new ManagerPage();
        public AccessPage page3 = new AccessPage();
        public SetBoxPage page5 = new SetBoxPage();
        public EditPwdPage page6 = new EditPwdPage();
        public PowerSettingPage page7 = new PowerSettingPage();
        #region 导航按钮
        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page1;
        }
        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page2;
        }
        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page3;
        }
        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page5;
        }
        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page6;
        }
        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            SetBorder(sender);
            Frame1.Content = page7;
        }
        public void SetBorder(object sender)
        {
            Button1.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button1.BorderThickness = new Thickness(1, 1, 1, 1);
            Button2.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button2.BorderThickness = new Thickness(1, 1, 1, 1);
            Button3.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button3.BorderThickness = new Thickness(1, 1, 1, 1);
            Button5.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button5.BorderThickness = new Thickness(1, 1, 1, 1);
            Button6.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button6.BorderThickness = new Thickness(1, 1, 1, 1);
            Button7.Foreground = (Brush)new BrushConverter().ConvertFromString("#000000");
            Button7.BorderThickness = new Thickness(1, 1, 1, 1);
            ((Button)sender).Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
            ((Button)sender).BorderThickness = new Thickness(0);
        }
        #endregion
    }
}
