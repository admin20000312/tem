﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DJ.Clients
{
    /// <summary>
    /// SuccessTip.xaml 的交互逻辑
    /// </summary>
    public partial class SuccessOpenBoxTip : Window
    {
        DispatcherTimer timer = new DispatcherTimer();
        bool boo { get; set; }
        public SuccessOpenBoxTip(int boxNum, bool bo = true)
        {
            InitializeComponent();
            ComWindow.Start(this);
            BoxNum = boxNum;
            boo = bo;
            if (bo) //存
            {
                mess.Content = BoxNum + "号箱门已打开,关闭箱门后开始充电!";
                VoiceHelper.GetInstance().Start(BoxNum + "号箱门已打开,关闭箱门后开始充电!");
            }
            else //取
            {
                mess.Content = BoxNum + "号箱门已打开,取出物品后随手关门!";
                VoiceHelper.GetInstance().Start(BoxNum + "号箱门已打开,取出物品后随手关门!");
            }
            timer.Interval = new TimeSpan(0, 0, 1);
            timer.Tick += ((a, b) =>
            {
                //检测箱门
                LockManager.GetInstance().CheckBox(BoxNum, (isclose) =>
                {
                    if (isclose == false)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            this.DialogResult = true;
                            Close();
                        });
                    }
                });
            });
            timer.IsEnabled = true;
            timer.Start();
        }
        public int BoxNum { get; set; }
        private void Window_Closed(object sender, EventArgs e)
        {
            timer.Stop();
        }
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var filepath = "pack://application:,,,/ClientImage/OpacityBtnOKBj.png";
            btn1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            btn1.IsEnabled = false;
            //检测箱门
            LockManager.GetInstance().CheckBox(BoxNum, (isclose) =>
            {
                if (isclose == false)
                {
                    Dispatcher.Invoke(() =>
                    {
                        this.DialogResult = true;
                        Close();
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        ////开启警报声音
                        //VoiceHelper.GetInstance().Start($"检测到{BoxNum}号箱门未关闭，请关闭箱门");
                        //Thread.Sleep(1000);
                        //btn1.Background = new ImageBrush(new BitmapImage(new Uri("pack://application:,,,/ClientImage/MainTimeSetOkBJ.png")));
                        //btn1.IsEnabled = true;

                        this.DialogResult = true;
                        this.Close();
                    });
                }
            });
        }
    }
}
