﻿using DJ.ZEF.ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DJ.Clients
{
    /// <summary>
    /// CloseBoxSuccess.xaml 的交互逻辑
    /// </summary>
    public partial class CloseBoxSuccess : Window
    {
       
        public CloseBoxSuccess(int boxNum,bool bo = true)
        {
            InitializeComponent();
            ComWindow.Start(this,Label1);
            if(bo) //存
            {
                mess.Content = boxNum + "号箱门已关闭，现在开始充电！";
                VoiceHelper.GetInstance().Start(boxNum + "号箱门已关闭，现在开始充电！");
            }
            else //取
            {
                mess.Content = boxNum + "号箱门已关闭，充电完成！";
                VoiceHelper.GetInstance().Start(boxNum + "号箱门已关闭，充电完成！");
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //this.DialogResult = true;
        }
    }
}
