﻿using DJ.Dialog;
using NSJL.Biz.Background.Quartzs;
using NSJL.Framework.Utils;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DJ.Pages
{
    /// <summary>
    /// PowerSettingPage.xaml 的交互逻辑
    /// </summary>
    public partial class PowerSettingPage : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public PowerSettingPage()
        {
            InitializeComponent();
            tbTime.Text = ini.readKey("TimeSet", "timeNum");
            startTime.lab1.Content = ini.readKey("TimeSet", "startTime");
            endTime.lab1.Content = ini.readKey("TimeSet", "endTime");
        }


        //设置
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string num = tbTime.Text.PadLeft(2,'0');
            ini.writeKey("TimeSet", "timeNum",num);
            ini.writeKey("TimeSet", "startTime", startTime.lab1.Content.ToString());
            ini.writeKey("TimeSet", "endTime", endTime.lab1.Content.ToString());
            var filepath = "pack://application:,,,/Image/组 212.png";
            btnSet.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            MessageDialog.ShowDialog("设置成功");
            filepath = "pack://application:,,,/Image/SetBtnBJ.png";
            btnSet.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));

            //动态修改调度器执行时间
            var start = ini.readKey("TimeSet", "startTime");
            DateTime dateTime = Convert.ToDateTime(start);
            int hour = dateTime.Hour;
            ITrigger startTrigger = TriggerBuilder.Create()
               .WithIdentity("trigger1", "group1")
               .WithCronSchedule($"0 0 {hour} * * ? *")
               .Build();
            TimingBiz.GetInstance().RescheduleJob(startTrigger);
            int hour1 = DateTime.Now.Hour;
            if (hour1 >= hour)
            {
                ini.writeKey("TimeSet", "AllowUsing", "1");
            }
            var end = ini.readKey("TimeSet", "endTime");
            dateTime = Convert.ToDateTime(end);
             hour = dateTime.Hour;
            ITrigger endTrigger = TriggerBuilder.Create()
               .WithIdentity("trigger2", "group2")
               .WithCronSchedule($"0 0 {hour} * * ? *")
               .Build();
            TimingBiz.GetInstance().RescheduleJob2(endTrigger);
            if (hour1 > hour)
            {
                ini.writeKey("TimeSet", "AllowUsing", "0");
            }
        }

        private void endTime_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            startTime.Height = 55;
            startTime.g1.Tag = "0";
            startTime.g2.Visibility = Visibility.Collapsed;
        }

        private void startTime_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            endTime.Height = 55;
            endTime.g1.Tag = "0";
            endTime.g2.Visibility = Visibility.Collapsed;
        }
    }
}
