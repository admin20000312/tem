﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DJ.Dialog;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NPOI.SS.Formula.Functions;
using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;

namespace DJ.Pages
{
    /// <summary>
    /// BoxPage.xaml 的交互逻辑
    /// </summary>
    public partial class BoxPage : Page
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public BoxPage()
        {
            InitializeComponent();
            Init();
        }
        public void Init()
        {
            var boxInfos = biz.GetBoxList().ToList();
            if (boxInfos != null && boxInfos.Count <= 0)
            {
                MessageDialog.ShowDialog("箱门为空");
                return;
            }
            WrapPanel1.Children.Clear();
            var count =Convert.ToInt32(ini.readKey("BoxConfig", "count"));
            for (int i = 0; i < count; i++)
            {
                var item = boxInfos[i];
                BoxControl btn = new BoxControl();
                btn.Width = 143;
                btn.Height = 125;
                btn.Margin = new Thickness(5, 5, 5, 5);
                btn.Label1.Content = item.BoxNum;
                btn.BoxNum = item.BoxNum;
                if (item.IsLock == true)
                {
                    btn.Label3.Content = "已锁定";
                    btn.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
                else
                {
                    btn.Label3.Content = "未锁定";
                }
                if (item.IsFree == true)
                {
                    btn.Label2.Text = "空闲";
                }
                else
                {
                    btn.Label2.Text = "存物";
                }
                btn.MouseDown += MouseButtonEventHandler;
                WrapPanel1.Children.Add(btn);
            }
            //foreach (var item in boxInfos)
            //{
            //    BoxControl btn = new BoxControl();
            //    btn.Width = 143;
            //    btn.Height = 125;
            //    btn.Margin = new Thickness(5, 5, 5, 5);
            //    btn.Label1.Content = item.BoxNum;
            //    btn.BoxNum = item.BoxNum;

            //    if (item.IsLock == true)
            //    {
            //        btn.Label3.Content = "已锁定";
            //        btn.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
            //    }
            //    else
            //    {
            //        btn.Label3.Content = "未锁定";
            //    }
            //    if (item.IsFree == true)
            //    {
            //        btn.Label2.Text = "空闲";
            //    }
            //    else
            //    {
            //        btn.Label2.Text = "存物";
            //    }
            //    btn.MouseDown += MouseButtonEventHandler;
            //    WrapPanel1.Children.Add(btn);
            //}
        }
        public void MouseButtonEventHandler(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxControl)sender;
            if (u.IsCheck)
            {
                u.IsCheck = false;
                var filepath = "pack://application:,,,/Image/a未选中.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                if (u.Label3.Content.ToString() != "已锁定")
                {
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                }
                else
                {
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
            }
            else
            {
                u.IsCheck = true;
                var filepath = "pack://application:,,,/Image/a选中.png";
                u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                if (u.Label3.Content.ToString() != "已锁定")
                {
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                }
                else
                {
                    u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                }
            }
        }
        //开箱
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count <= 0)
            {
                MessageDialog.ShowDialog("请选择箱门");
                return;
            }
            string str = string.Join(",", list.ToArray());
            Task.Run(() =>
            {
                foreach (var item in list)
                {
                    LockManager.GetInstance().OpenBox(item);
                    Thread.Sleep(600);
                }
            });

            var tip = str + "号箱门开箱成功";
            VoiceHelper.GetInstance().Start(tip);
            biz.EditAdminLogInfo(tip, Main.UserName);
            MessageDialog.ShowDialog(str + "开箱成功");

        }
        //全开
        private void Button_Click1(object sender, RoutedEventArgs e)
        {
            MessageDialog mes = new MessageDialog();
            mes.TextBlock1.Text = "是否确定操作";
            if (mes.ShowDialog() != true)
            {
                return;
            }
            Task.Run(() =>
            {
                var list = biz.GetBoxList();
                foreach (var item in list)
                {
                    LockManager.GetInstance().OpenBox(item.BoxNum);
                    Thread.Sleep(600);
                }
            });

            var tip = "全开箱门操作成功";
            VoiceHelper.GetInstance().Start(tip);

            biz.EditAdminLogInfo(tip, Main.UserName);
            MessageDialog.ShowDialog("全开箱门操作成功");
        }
        //清箱
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            //var list = GetSelectBox();

        }
        //全清
        private void Button_Click3(object sender, RoutedEventArgs e)
        {
            //MessageDialog mes = new MessageDialog();
            //mes.TextBlock1.Text = "是否确定操作";
            //if (mes.ShowDialog() != true)
            //{
            //    return;
            //}


        }
        CommBiz biz = new CommBiz();
        //锁箱
        private void Button_Click4(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count <= 0)
            {
                MessageDialog.ShowDialog("请选择箱门");
                return;
            }
            biz.EditLockBoxs(list, true);
            Init();


        }
        //解锁
        private void Button_Click5(object sender, RoutedEventArgs e)
        {
            var list = GetSelectBox();
            if (list.Count <= 0)
            {
                MessageDialog.ShowDialog("请选择箱门");
                return;
            }
            biz.EditLockBoxs(list, false);
            Init();
        }
        private List<int> GetSelectBox()
        {
            List<int> list = new List<int>();
            foreach (BoxControl item in WrapPanel1.Children)
            {
                if (item.IsCheck)
                {
                    list.Add(item.BoxNum);
                }
            }
            return list;
        }

        private void Page_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Init();
        }
        //全选
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Button click = (Button)sender;
            var name = (string)click.Content;


            foreach (BoxControl item in WrapPanel1.Children)
            {
                var u = item;
                if (name != "全选")
                {
                    click.Content = "全选";

                    u.IsCheck = false;

                    var filepath = "pack://application:,,,/Image/a未选中.png";
                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    if (u.Label3.Content.ToString() != "已锁定")
                    {
                        u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#1A1A1A");
                    }
                    else
                    {
                        u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                    }

                }
                else
                {
                    click.Content = "不选";

                    u.IsCheck = true;

                    var filepath = "pack://application:,,,/Image/a选中.png";

                    u.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    u.Label1.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    u.Label2.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    if (u.Label3.Content.ToString() != "已锁定")
                    {
                        u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#3CAAE5");
                    }
                    else
                    {
                        u.Label3.Foreground = (Brush)new BrushConverter().ConvertFromString("#ff0000");
                    }
                }
            }
        }

        private void SCManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        //强制归还
        private void Button_Click_32(object sender, RoutedEventArgs e)
        {
            var biz = new CommBiz();
            var list = GetSelectBox();

        }
    }
}
