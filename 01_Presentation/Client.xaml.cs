﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using DJ.Clients;
using DJ.Dialog;
using DJ.UserControls;
using DJ.ZEF.ThirdParty;
using NPOI.HSSF.Record;
using NPOI.SS.Formula.Functions;
using NSJL.Biz.Background.Client;
using NSJL.Biz.Background.Quartzs;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;

namespace DJ
{
    /// <summary>
    /// Client.xaml 的交互逻辑
    /// </summary>
    public partial class Client : Window
    {
        CommBiz biz = new CommBiz();
        IniFile ini = new IniFile("Config/Config.ini");

        public Client()
        {
            InitializeComponent();
            ComWindow.Start(this);
            TextLogUtil.Info("启动成功");
            time = Convert.ToInt32(ini.readKey("TimeSet", "timeNum"));
            currentTop = Canvas.GetTop(g1);
            ini.writeKey("RestartApp", "token", "0");
        }

        public void Init()
        {
            List<BoxInfo> list = biz.GetBoxList();
            if (list.Count <= 0)
            {
                MessageDialog.ShowDialog("当前箱柜没有箱门");
                return;
            }
            wp1.Children.Clear();
            var count = Convert.ToInt32(ini.readKey("BoxConfig", "count"));
            for (int i = 0; i < count; i++)
            {
                BoxInfo box = list[i];
                BoxClient b = new BoxClient();
                b.Width = 218;
                b.Height = 120;
                b.Margin = new Thickness(5, 10, 5, 10);
                b.boxnum.Content = box.BoxNum;
                b.MouseDown += B_MouseDown;
                if ((bool)box.IsFree) //空闲
                {
                    //图片地址设置背景图
                    var filepath = "pack://application:,,,/ClientImage/NoUsedBtnBj.png";
                    b.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    filepath = "pack://application:,,,/ClientImage/CheckSelectBlue.png";
                    b.selectBox.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    b.state.Content = "空闲";
                }
                else
                {
                    //图片地址设置背景图
                    var filepath = "pack://application:,,,/ClientImage/UsedBtnBj.png";
                    b.Grid1.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    filepath = "pack://application:,,,/ClientImage/CheckSelectYellow.png";
                    b.selectBox.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
                    b.state.Content = "占用";
                }
                wp1.Children.Add(b);
            }
        }
        public int SelectBox { get; set; }
        public void B_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var u = (BoxClient)sender;
            var children = wp1.Children;
            foreach (var btn in children)
            {
                BoxClient boxClient = btn as BoxClient;
                if (btn != u)
                {
                    boxClient.selectBox.Visibility = Visibility.Hidden;
                    boxClient.IsCheck = false;
                }
            }
            if (u.IsCheck) //取消选中
            {
                u.IsCheck = false;
                SelectBox = 0;
                u.selectBox.Visibility = Visibility.Hidden;
            }
            else //选中
            {
                SelectBox = Convert.ToInt32(u.boxnum.Content);
                u.IsCheck = true;
                u.selectBox.Visibility = Visibility.Visible;
            }
        }
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                Login login = new Login();
                login.ShowDialog();
            }
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            LockManager.GetInstance().Close();
            HlacHelper.GetInstance().Close();
            Environment.Exit(0);
        }

        private void ScrollViewer_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }

        public void Animal(double opacity, bool b, int topNum)
        {
            var filepath = "pack://application:,,,/ClientImage/MainTimeSetOkBJ.png";
            btnOk.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            wp1.Opacity = opacity;
            wp1.IsEnabled = b;
            // 计算控件目标位置
            double targetTop = currentTop - topNum; // 例如向右移动100个单位（“+”是向下，“-”是向上）

            // 创建动画对象
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = currentTop;
            animation.To = targetTop;
            animation.Duration = TimeSpan.FromMilliseconds(500); //动画持续3秒

            // 设置动画的目标属性
            Storyboard.SetTarget(animation, g1);
            Storyboard.SetTargetProperty(animation, new PropertyPath(Canvas.TopProperty));

            // 创建并启动故事板
            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(animation);
            storyboard.Begin(canvas);
            if (b)
            {
                tb1.Text = string.Empty;
            }
        }

        double currentTop;
        //存
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var IsAllow = ini.readKey("TimeSet", "AllowUsing");
                if (IsAllow != "1")
                {
                    MessageDialog.ShowDialog("不在使用时间段，不能进行存操作");
                    VoiceHelper.GetInstance().Start("不在使用时间段，不能进行存操作");
                    return;
                }
                if (SelectBox == 0)
                {
                    MessageDialog.ShowDialog("请先选择箱门");
                    return;
                }
                Animal(0.61, false, 438);
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                MessageDialog.ShowDialog(ex.Message);
            }
        }
        //取
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                if (SelectBox == 0)
                {
                    MessageDialog.ShowDialog("请先选择箱门");
                    return;
                }
                LockManager.GetInstance().OpenBox(SelectBox);
                //开箱断电
                HlacHelper.GetInstance().OpenOrCut((byte)SelectBox, 0x00, (s) =>
                {
                    if ((bool)s)
                    {
                        var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                        TextLogUtil.Info(now + "———" + SelectBox + "号箱门开箱取断电成功");
                    }
                    else
                    {
                        var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                        TextLogUtil.Info(now + "———" + SelectBox + "号箱门开箱取断电失败");

                    }
                });
                OpenBoxInfo openInfo = new OpenBoxInfo() { Id = Guid.NewGuid().ToString("N"), BoxNum = SelectBox, Type = "取", CreateTime = DateTime.Now };
                var commonResult = biz.EditOpenBoxInfo(openInfo);
                if (!commonResult.result)
                {
                    TextLogUtil.Info("开箱记录保存失败：" + commonResult.message);
                }
                SuccessOpenBoxTip st = new SuccessOpenBoxTip(SelectBox, false);
                if (st.ShowDialog() == true)
                {
                    CloseBoxSuccess cbs = new CloseBoxSuccess(SelectBox, false);
                    cbs.Show();
                    //修改箱门状态
                    commonResult = biz.EditBoxSate(SelectBox, true);
                    if (!commonResult.result)
                    {
                        TextLogUtil.Info("箱门状态修改异常：" + commonResult.message);
                    }
                    Init();
                    bool v = dic.ContainsKey(SelectBox);
                    if (v)
                    {
                        DispatcherTimer t = dic[SelectBox];
                        t.Stop();
                    }
                    SelectBox = 0;
                    //Task.Run(() =>
                    //{
                    //    //开始断电
                    //    HlacHelper.GetInstance().OpenOrCut((byte)SelectBox, 0x00, (s) =>
                    //    {
                    //        if ((bool)s)
                    //        {
                    //            var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                    //            TextLogUtil.Info(now + "———" + SelectBox + "号箱门断电成功");
                    //        }
                    //    });
                    //});
                }
            }
            catch (Exception ex)
            {
                TextLogUtil.Info(ex.Message);
                MessageDialog.ShowDialog(ex.Message);
            }
        }


        int time { get; set; }
        //确定
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var filepath = "pack://application:,,,/ClientImage/OpacityBtnOKBj.png";
            btnOk.Background = new ImageBrush(new BitmapImage(new Uri(filepath)));
            string text = tb1.Text;
            if (!string.IsNullOrEmpty(text) && text != "设置通电时长")
            {
                bool v = int.TryParse(text, out int res);
                if (v)
                {
                    time = res;
                }
                else
                {
                    MessageDialog.ShowDialog("输入的格式不正确");
                    return;
                }
            }
            LockManager.GetInstance().OpenBox(SelectBox);
            //开箱断电
            HlacHelper.GetInstance().OpenOrCut((byte)SelectBox, 0x00, (s) =>
            {
                if ((bool)s)
                {
                    var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                    TextLogUtil.Info(now + "———" + SelectBox + "号箱门开箱存断电成功");
                }
            });
            OpenBoxInfo openInfo = new OpenBoxInfo() { Id = Guid.NewGuid().ToString("N"), BoxNum = SelectBox, Type = "存", CreateTime = DateTime.Now };
            var commonResult = biz.EditOpenBoxInfo(openInfo);
            if (!commonResult.result)
            {
                TextLogUtil.Info("开箱记录保存失败：" + commonResult.message);
            }
            SuccessOpenBoxTip st = new SuccessOpenBoxTip(SelectBox);
            if (st.ShowDialog() == true)
            {
                Animal(1, true, 0);
                CloseBoxSuccess cbs = new CloseBoxSuccess(SelectBox);
                cbs.Show();
                //修改箱门状态
                commonResult = biz.EditBoxSate(SelectBox, false);
                if (!commonResult.result)
                {
                    TextLogUtil.Info("箱门状态修改异常：" + commonResult.message);
                }
                Init();
                //开始通电
                HlacHelper.GetInstance().OpenOrCut((byte)SelectBox, 0x01, (s) =>
                {

                    if ((bool)s)
                    {
                        var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                        TextLogUtil.Info(now + "———" + SelectBox + "号箱门通电成功");
                    }
                    else
                    {
                        var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                        TextLogUtil.Info(now + "———" + SelectBox + "号箱门通电失败");
                    }
                });
                bool v = dic.ContainsKey(SelectBox);
                if (!v)
                {
                    DispatcherTimer ti = new DispatcherTimer();
                    ti.Tag = SelectBox;
                    ti.Interval = new TimeSpan(0, 0, time);
                    ti.Tick += Ti_Tick;
                    ti.IsEnabled = true;
                    ti.Start();
                    dic.Add(SelectBox, ti);
                }
                else
                {
                    DispatcherTimer t = dic[SelectBox];
                    t.Interval = new TimeSpan(0, 0, time);
                    t.Start();
                }
                SelectBox = 0;
            }
        }

        private void Ti_Tick(object sender, EventArgs e)
        {
            DispatcherTimer dispatcherTimer = sender as DispatcherTimer;
            int num = Convert.ToInt32(dispatcherTimer.Tag);
            //MessageBox.Show(num.ToString());
            dispatcherTimer.Stop();
            //断电
            HlacHelper.GetInstance().OpenOrCut((byte)num, 0x00, (s) =>
            {
                if ((bool)s)
                {
                    var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                    TextLogUtil.Info(now + "———" + SelectBox + "号箱门断电成功");
                }
                else
                {
                    var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                    TextLogUtil.Info(now + "———" + SelectBox + "号箱门断电失败");
                }
            });
        }

        private void border1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Animal(1, true, 0);
        }


        /// <summary>
        /// 创建每个箱门的定时器
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        Dictionary<int, DispatcherTimer> dic = new Dictionary<int, DispatcherTimer>();

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Interval = new TimeSpan(0, 5, 0);
            timer1.Tick += ((a, b) =>
            {
                Init();
            });
            timer1.IsEnabled = true;
            timer1.Start();
            Task.Run(() =>
            {
                try
                {
                    #region 锁板串口开启,只启动一次
                    var ini = new IniFile("Config/Config.ini");
                    var portName = ini.readKey("BoxConfig", "portName");
                    var baudRate = ini.readKey("BoxConfig", "baudRate");
                    var result = LockManager.GetInstance().Start(portName, baudRate.ToInt32());
                    if (!result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result.message);
                        });
                    }
                    #endregion
                    #region 电控板串口开启,只启动一次
                    var com = ini.readKey("PowerConfig", "portName");
                    var bt = ini.readKey("PowerConfig", "baudRate");
                    result = HlacHelper.GetInstance().Start(com, bt.ToInt32());
                    if (!result.result)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            MessageDialog.ShowDialog(result.message);
                        });
                    }
                    #endregion
                    #region 调取器开启，定时通电、断电
                    var startTime = ini.readKey("TimeSet", "startTime");
                    DateTime dateTime = Convert.ToDateTime(startTime);
                    int hour = dateTime.Hour;
                    TimingBiz.GetInstance().Start(hour);

                    var endTime = ini.readKey("TimeSet", "endTime");
                    dateTime = Convert.ToDateTime(endTime);
                    hour = dateTime.Hour;
                    TimingBiz.GetInstance().Start2(hour);
                    #endregion
                }
                catch (Exception ex)
                {
                    TextLogUtil.Info(ex.Message);
                }
            });
        }
    }
}

