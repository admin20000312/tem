﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class OpenBoxInfo
    {
        [Key]
        public string Id { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        ////箱门里的插口号
        //public int SocketNum { get; set; }
        //操作类型   存   取
        public string Type { get; set; }
        //创建时间
        public DateTime? CreateTime { get; set; }
       
    }
}
