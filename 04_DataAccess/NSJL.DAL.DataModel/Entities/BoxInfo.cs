﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.DAL.DataModel.Entities
{
    public class BoxInfo
    {
        [Key]
        public string Id { get; set; }
        ////箱门通电状态：通电、断电
        //public string PowerState { get; set; }
        //箱门编号
        public int BoxNum { get; set; }
        
        //是否锁定   默认false
        public bool? IsLock { get; set; }
        public bool? IsFree { get; set; }

        //最后开门时间
        public DateTime? UpdateTime { get; set; }
        //绑定时间
        public DateTime? CreateTime { get; set; }
    }
}