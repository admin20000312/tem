﻿using NSJL.DAL.DataAccess;
using NSJL.DAL.DataModel.Entities;
using NSJL.DomainModel.Background.System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using NSJL.DomainModel.Background.Client;
using NSJL.Framework.Utils;
using DJ.ZEF.ThirdParty;
using System.Data.Entity.Migrations;

namespace NSJL.Biz.Background.Client
{
    public class CommBiz
    {
        public string CabinetName = null;
        public CommBiz()
        {
            var ini = new IniFile("Config/Config.ini");
            CabinetName = ini.readKey("BoxConfig", "cabinetName");
        }
        public SJLDbContext db = new SJLDbContext();
        public CommonResult Login(string username, string password)
        {

            var info = db.AdminInfo.Where(p => p.UserName == username).FirstOrDefault();
            if (info == null)
            {
                return new CommonResult() { result = false, message = "用户不存在" };
            }
            if (info.Password != password)
            {
                return new CommonResult() { result = false, message = "密码不正确" };
            }
            return new CommonResult() { result = true };
        }
        public CommonResult EditPassword(string username, string old, string news)
        {
            var db = new SJLDbContext();
            var info = db.AdminInfo.Where(p => p.UserName == username).FirstOrDefault();
            if (info.Password != old)
            {
                return new CommonResult() { result = false, message = "老密码错误" };
            }

            info.Password = news;
            db.SaveChanges();
            return new CommonResult() { result = true, message = "操作成功" };
        }


        public bool InitBoxInfo(int count)
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            db.BoxInfo.RemoveRange(list);
            for (int i = 1; i <= count; i++)
            {
                var info = new BoxInfo();
                info.Id = Guid.NewGuid().ToString("N");
                info.CreateTime = DateTime.Now;
                info.UpdateTime = DateTime.Now;
                info.BoxNum = i;
                info.IsFree = true;
                info.IsLock = false;
                //info.PowerState = "断电";
                db.BoxInfo.Add(info);
            }
            db.SaveChanges();
            return true;
        }



        public List<OpenBoxInfo> GetOpenBoxList(string ope, DateTime? start, DateTime? end)
        {
            var sql = db.OpenBoxInfo.ToList();
            if (start != null)
            {
                sql = sql.Where(p => p.CreateTime > start).ToList();
            }
            if (end != null)
            {
                sql = sql.Where(p => p.CreateTime < end).ToList();
            }
            if (!string.IsNullOrWhiteSpace(ope))
            {
                sql = sql.Where(p => p.Type == ope).ToList();
            }
            return sql.OrderByDescending(p => p.CreateTime).ToList();
        }
        public List<AdminLogInfo> GetAdminLogList(DateTime? start, DateTime? end)
        {
            var db = new SJLDbContext();
            if (start == null)
            {
                return db.AdminLogInfo.OrderByDescending(p => p.CreateTime).ToList();
            }
            else
            {
                return db.AdminLogInfo.Where(p => p.CreateTime >= start && p.CreateTime <= end).OrderByDescending(p => p.CreateTime).ToList();
            }
        }
        public List<BoxInfo> GetBoxList()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            return list;
        }
        public List<GetBoxWithExcel> GetBoxListWithExcel()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetBoxWithExcel>>(list);
        }
        public List<GetGoodsList> GetBoxListWithLin()
        {
            var db = new SJLDbContext();
            var list = db.BoxInfo.OrderBy(p => p.BoxNum).ToList();
            return AutoMapper.Mapper.DynamicMap<List<GetGoodsList>>(list);
        }
        public bool EditAdminLogInfo(string str, string adminname)
        {
            var db = new SJLDbContext();
            var info = new AdminLogInfo();
            info.UserName = adminname;
            info.CreateTime = DateTime.Now;
            info.Content = str;
            info.Id = Guid.NewGuid().ToString("N");
            db.AdminLogInfo.Add(info);
            db.SaveChanges();
            return true;
        }



        #region new
        public CommonResult EditOpenBoxInfo(OpenBoxInfo openInfo)
        {
            try
            {
                db.OpenBoxInfo.Add(openInfo);
                db.SaveChanges();
                return new CommonResult() { result = true };
            }
            catch (Exception ex)
            {
                return new CommonResult() { result = false, message = ex.Message };
            }
        }


        public void EditLockBoxs(List<int> list, bool lockState)
        {
            try
            {
                foreach (int i in list)
                {
                    BoxInfo boxInfo = db.BoxInfo.Where(p => p.BoxNum == i).FirstOrDefault();
                    boxInfo.IsLock = lockState;
                    db.BoxInfo.AddOrUpdate(boxInfo);
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                TextLogUtil.Info("编辑箱门锁箱状态错误：" + ex.Message);
            }
        }
        public CommonResult EditBoxSate(int boxNum, bool state)
        {
            try
            {
                BoxInfo boxInfo = db.BoxInfo.Where(p => p.BoxNum == boxNum).FirstOrDefault();
                boxInfo.IsFree = state;
                db.BoxInfo.AddOrUpdate(boxInfo);
                db.SaveChanges();
                return new CommonResult() { result = true};
            }
            catch (Exception ex)
            {
                TextLogUtil.Info("修改箱门状态错误：" + ex.Message);
                return new CommonResult() { result = false,message = ex.Message };
            }
        }
        #endregion
    }
}
