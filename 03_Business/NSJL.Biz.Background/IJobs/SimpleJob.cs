﻿using NSJL.Biz.Background.Client;
using NSJL.Framework.Utils;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Biz.Background.IJobs
{
    public class SimpleJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }

        }
    }
}