﻿using NSJL.Framework.Utils;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Biz.Background.IJobs
{
    //通电
    public class StartPower : IJob
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                ini.writeKey("TimeSet", "AllowUsing", "1");
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
    }
}
