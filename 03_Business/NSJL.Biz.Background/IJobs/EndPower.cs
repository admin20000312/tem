﻿using NSJL.Framework.Utils;
using NSJL.Plugin.Third.ThirdParty;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSJL.Biz.Background.IJobs
{
    //断电
    public class EndPower:IJob
    {
        IniFile ini = new IniFile("Config/Config.ini");
        public void Execute(IJobExecutionContext context)
        {
            try
            {
                ini.writeKey("TimeSet", "AllowUsing", "0");
                var count = Convert.ToInt32(ini.readKey("BoxConfig", "count"));
                for (int i = 1; i <= count; i++)
                {
                    HlacHelper.GetInstance().OpenOrCut((byte)i, 0x00, (s) =>
                    {
                        if ((bool)s)
                        {
                            var now = DateTime.Now.ToString("yyyy-MM-dd HH");
                            TextLogUtil.Info(now + $"——{i}号箱门断电成功");
                        }
                        else
                        {
                            Environment.Exit(0);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                TextLogUtil.Info(e.Message);
            }
        }
    }
}
