/*
 Navicat Premium Data Transfer

 Source Server         : DJ
 Source Server Type    : MariaDB
 Source Server Version : 100515
 Source Host           : LOCALHOST:3306
 Source Schema         : dj_tem

 Target Server Type    : MariaDB
 Target Server Version : 100515
 File Encoding         : 65001

 Date: 02/04/2024 10:56:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admininfo
-- ----------------------------
DROP TABLE IF EXISTS `admininfo`;
CREATE TABLE `admininfo`  (
  `Id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UserName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Password` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CreateTime` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admininfo
-- ----------------------------
INSERT INTO `admininfo` VALUES ('1', 'admin', '000000', '2023-09-25 10:47:48');

-- ----------------------------
-- Table structure for adminloginfo
-- ----------------------------
DROP TABLE IF EXISTS `adminloginfo`;
CREATE TABLE `adminloginfo`  (
  `Id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `UserName` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '管理员名',
  `Content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作内容',
  `CreateTime` datetime NULL DEFAULT NULL COMMENT '操作时间'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of adminloginfo
-- ----------------------------
INSERT INTO `adminloginfo` VALUES ('1', 'admin', 'dddd', '2024-03-21 17:13:41');

-- ----------------------------
-- Table structure for boxinfo
-- ----------------------------
DROP TABLE IF EXISTS `boxinfo`;
CREATE TABLE `boxinfo`  (
  `Id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BoxNum` int(11) NULL DEFAULT NULL COMMENT '箱门号',
  `IsFree` tinyint(2) NULL DEFAULT NULL COMMENT '是否空闲（1：表示空闲，0：表示否）',
  `IsLock` tinyint(2) NULL DEFAULT NULL COMMENT '是否锁定  1表示锁定',
  `UpdateTime` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `CreateTime` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of boxinfo
-- ----------------------------
INSERT INTO `boxinfo` VALUES ('0d82f86045f3471ca73a1e22a59362bf', 6, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('3d65e319d1c04979b5f347800831fbc6', 9, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('4552c97ff8964e1799b9a673ff1015e7', 7, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('7609cf83d9794ab7b5c7e489c35df605', 3, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('863d0ad454c64215b2b1df62d4f932e8', 2, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('bf074b6a6303429f95d0523f8a1a3ffc', 4, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('c3f921227e91493da4d0ef51080ae41f', 8, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('d140272fe15f47bfa6c9f3230c628dad', 10, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('d2d1762c3ac9446ba268a20b6905812c', 5, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('e07ff695e6af4eab9f3b5d7e2795ac37', 11, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');
INSERT INTO `boxinfo` VALUES ('e7ef1465ea254e2896246cf3b4e379ba', 1, 1, 0, '2024-04-01 17:16:31', '2024-04-01 17:16:31');

-- ----------------------------
-- Table structure for openboxinfo
-- ----------------------------
DROP TABLE IF EXISTS `openboxinfo`;
CREATE TABLE `openboxinfo`  (
  `Id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `BoxNum` int(11) NULL DEFAULT NULL COMMENT '箱门号',
  `Type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型：存、取',
  `CreateTime` datetime NULL DEFAULT NULL COMMENT '时间',
  PRIMARY KEY (`Id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of openboxinfo
-- ----------------------------
INSERT INTO `openboxinfo` VALUES ('41a23f8402d346ce939a7a6f8b73b4f4', 1, '存', '2023-10-21 10:20:30');
INSERT INTO `openboxinfo` VALUES ('48c62c1fd4734b0ba209100d69d02af7', 1, '存', '2023-10-17 13:29:13');
INSERT INTO `openboxinfo` VALUES ('ae1907402f9c4cc4b3d1acb5339f9adb', 1, '存', '2023-10-21 10:20:18');
INSERT INTO `openboxinfo` VALUES ('b7f58f3dc99b4ab79950f4afd579e544', 1, '存', '2023-10-23 10:31:13');
INSERT INTO `openboxinfo` VALUES ('e7e51735d1884bd295ee033eda87c5a1', 1, '存', '2023-10-17 13:29:21');
INSERT INTO `openboxinfo` VALUES ('ee1cf07b56c941958b99576c3992a04e', 1, '存', '2023-10-23 09:37:29');
INSERT INTO `openboxinfo` VALUES ('f2dd7f2c8bc141889db76f4db12c03fd', 1, '存', '2023-10-23 10:35:58');
INSERT INTO `openboxinfo` VALUES ('f69816acdae54c12b4f89df8a372f47a', 1, '存', '2023-10-23 09:37:43');

SET FOREIGN_KEY_CHECKS = 1;
